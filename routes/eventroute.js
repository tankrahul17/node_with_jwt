var json = {};
var jwt = require('jsonwebtoken');
var CONSTANT = require('../config/constant.json');
var COMMON_SERVICES = require('./commonroute');
var model = require('../models/model');
var EVENTS_COLLECTION = model.events;

var ObjectID = require('mongodb').ObjectID;
/*-------------------------------------------------------*/
exports.addEvent = _addEvent;
exports.getEventUsersBydate = _getEventUsersBydate;
/*-------------------------------------------------------*/

function _addEvent(req, res, next) {
  var json = {};

  var name = req.body.name;
  var start_date_time = req.body.start_date_time;
  var end_date_time = req.body.end_date_time;
  var user_id = req.body.user_id;
  var type = req.body.type;

  if (COMMON_SERVICES.isUndefinedOrNull(name) || COMMON_SERVICES.isUndefinedOrNull(start_date_time) || COMMON_SERVICES.isUndefinedOrNull(end_date_time) || COMMON_SERVICES.isUndefinedOrNull(user_id) || COMMON_SERVICES.isUndefinedOrNull(type)) {
    json.status = '0';
    json.result = { 'Message': 'Required param is missing' };
    res.send(json);
  } else if (!COMMON_SERVICES.isValidId(user_id)) {
    json.status = '0';
    json.result = { 'Message': 'Please send valid Id' };
    res.send(json);
  } else if (!COMMON_SERVICES.chkDateInWeekDay(end_date_time) || !COMMON_SERVICES.chkDateInWeekDay(start_date_time)) {
    json.status = '0';
    json.result = { 'Message': 'Start date or End date is not valid!!' };
    res.send(json);
  } else {

    var query = { $and: [{ type: type }, { end_date_time: end_date_time }, { start_date_time: start_date_time }, { user_id: new ObjectID(user_id) }, { name: name }] };
    EVENTS_COLLECTION.find(query, function (eventerror, getEvent) {
      if (eventerror || getEvent.length > 0) {
        json.status = '0';
        json.result = { 'Message': 'Event Already Exists' };
        res.send(json);
      } else {
        var event = new EVENTS_COLLECTION({
          type: type,
          end_date_time: COMMON_SERVICES.formatyyyyMMDDTHHmmss(end_date_time),
          start_date_time: COMMON_SERVICES.formatyyyyMMDDTHHmmss(start_date_time),
          user_id: user_id,
          name: name
        });
        event.save(function (error, user) {
          if (error) {
            json.status = '0';
            json.result = { 'error': 'Error In Adding Event' };
            res.send(json);
          } else {
            json.status = '1';
            json.result = { 'Message': 'Event added successfully!' };
            res.send(json);
          }
        });
      }
    });
  }

}


/**
 * POST: Get events
 * @param name Username. 
*/

function _getEventUsersBydate(req, res, next) {
  var json = {};
  var date = req.body.date;
  if (COMMON_SERVICES.isUndefinedOrNull(date)) {
    json.status = '0';
    json.result = { 'Message': 'Date is missing!!' };
    res.send(json);
  } else {
    date = COMMON_SERVICES.getFormatedDate(date);
    var query = { start_date_time: { $regex: date } };
    EVENTS_COLLECTION.find(query, function (eventerror, eventsDetails) {
      if (eventerror) {
        json.status = '0';
        json.result = { 'Message': 'Something went worng!!' + JSON.stringify(eventerror) };
        res.send(json);
      } else {
        json.status = '1';
        json.result = eventsDetails;
        res.send(json);
      }
    });
  }
}
