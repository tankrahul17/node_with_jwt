var json = {};
var jwt = require('jsonwebtoken');
var CONSTANT = require('../config/constant.json');
var COMMON_SERVICES = require('./commonroute');
var model = require('../models/model');
var USER_COLLECTION = model.user;

var ObjectID = require('mongodb').ObjectID;
/*-------------------------------------------------------*/
exports.getUserToken = _getUserToken;
exports.getUsers = _getUsers;
exports.addUsers = _addUsers;
exports.removeUserById = _removeUserById;
exports.updateUserById = _updateUserById;

/*-------------------------------------------------------*/

/*
TODO:To Get User Token
*/
function _getUserToken(req, res, next) {
    var email = req.body.email;
    var User = { "email": email };

    var token = jwt.sign(User, CONSTANT.superSecret, {
        expiresIn: 86400 // expires in 24 hours
    });
    json.status = '1';
    json.result = { "user": User, "token": 'Basic ' + token };
    res.send(json);
}


/**
 * POST: Get users
 * @param name Username. 
*/

function _getUsers(req, res, next) {
    var json = {};
    var query = {};
    USER_COLLECTION.find(query, function (usererror, users) {
        if (usererror || users.length <= 0) {
            json.status = '0';
            json.result = { 'Message': 'User Not Found' };
            res.send(json);
        } else {
            json.status = '1';
            json.result = users;
            res.send(json);
        }
    });
}

/**
 * TODO: Add users 
*/

function _addUsers(req, res, next) {
    var json = {};

    var name = req.body.name;
    var email = req.body.email;
    var type = req.body.type;

    var user = new USER_COLLECTION({
        name: name,
        email: email,
        type: type
    });

    var query = { email: email };
    USER_COLLECTION.find(query, function (usererror, getUser) {
        if (usererror || getUser.length > 0) {
            json.status = '0';
            json.result = { 'Message': 'User Already Exists' };
            res.send(json);
        } else {
            user.save(function (error, user) {
                if (error) {
                    json.status = '0';
                    json.result = { 'error': 'Error In Adding New User' };
                    res.send(json);
                } else {
                    json.status = '1';
                    json.result = { 'Message': 'User added successfully!' };
                    res.send(json);
                }
            });
        }
    });
}



/*
TODO: POST To Remove User By Id
*/
function _removeUserById(req, res, next) {
    var user_id = req.params.id;
    USER_COLLECTION.deleteOne({ _id: new ObjectID(user_id) }, function (error, result) {
        if (error) {
            json.status = '0';
            json.result = { 'error': 'Error In Removing New User' };
            res.send(json);
        } else {
            json.status = '1';
            json.result = { 'Message': 'User removed successfully!' };
            res.send(json);
        }
    });
}


/*
TODO: POST To Update User By Id
*/
function _updateUserById(req, res, next) {
    var user_id = req.params.id;

    var email = req.body.email;
    var name = req.body.name;
    var type = req.body.type;

    var query = {
        $set: {
            name: name,
            email: email,
            type: type
        }
    };

    USER_COLLECTION.find({ email: email }, function (usererror, getUser) {
        if (usererror || (getUser.length > 0 && getUser[0]._id != user_id)) {
            json.status = '0';
            json.result = { 'Message': 'User Already Exists' };
            res.send(json);
        } else {
            USER_COLLECTION.update({ _id: new ObjectID(user_id) }, query, function (error, result) {
                if (error) {
                    json.status = '0';
                    json.result = { 'error': 'Error In Updating New User' };
                    res.send(json);
                } else {
                    json.status = '1';
                    json.result = { 'Message': 'User updated successfully!' };
                    res.send(json);
                }
            });
        }
    });
}

