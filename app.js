var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var CONSTANT = require('./config/constant.json');
var database = require('./config/database'); 	// Get configuration file
var mongoose = require('mongoose');
var http = require('http');

var config = require('./config/config');
var indexRouter = require('./routes/index');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.set('superSecret', CONSTANT.superSecret);
app.set('port', process.env.PORT || config.api_server_port);


app.use(logger('dev'));
var jwt = require('jsonwebtoken');
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));


/***************************************** headers */

// Add headers
app.use(function (req, res, next) {

  // Website you wish to allow to connect
  res.setHeader('Access-Control-Allow-Origin', '*');

  // Request methods you wish to allow
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

  // Request headers you wish to allow
  // res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type,Authorization ,Accept');

  res.setHeader("Access-Control-Allow-Headers", "Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers");

  // res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type,Authorization ,Accept');

  // Set to true if you need the website to include cookies in the requests sent
  // to the API (e.g. in case you use sessions)
  res.setHeader('Access-Control-Allow-Credentials', true);

  // Pass to next layer of middleware
  next();
});

/***************************************** authentication */

// API ROUTES -------------------

// get an instance of the router for api routes
var apiRoutes = express.Router();

// route middleware to verify a token
apiRoutes.use(function (req, res, next) {

  // check header or url parameters or post parameters for token
  var token = req.body.token || req.query.token || req.headers.authorization;

  // decode token
  if (token) {
    token = token.split(" ")[1];
    // console.log('toke' + token + ' app.get() ' + app.get('superSecret'));
    // verifies secret and checks exp
    jwt.verify(token, app.get('superSecret'), function (err, decoded) {
      if (err) {
        return res.json({ success: 0, message: 'Failed to authenticate token.' });
      } else {
        // if everything is good, save to request for use in other routes
        req.decoded = decoded;
        next();
      }
    });

  } else {

    // if there is no token
    // return an error
    return res.status(403).send({
      success: 0,
      message: 'No token provided.'
    });

  }
});

app.use('/api', apiRoutes);

/*-----------------------All Routes List---------------------------------*/
var userroute = require('./routes/userroute');
var eventroute = require('./routes/eventroute');

/*--------------------------- User Routes------------------------------*/
app.post('/user/getUserToken', userroute.getUserToken);
app.get('/api/user/getUsers', userroute.getUsers);
app.post('/api/user/addUser', userroute.addUsers);
app.post('/api/user/removeUserById/:id', userroute.removeUserById);
app.post('/api/user/updateUserById/:id', userroute.updateUserById); 

/*--------------------------- Event Routes------------------------------*/ 
app.post('/api/event/addEvent', eventroute.addEvent);  
app.post('/api/event/getEventUsersBydate', eventroute.getEventUsersBydate);  


/*--------------------------- API Guide------------------------------*/
app.use('/', indexRouter);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

//Connection with Database
var databaseURL = database.localdburl;
var mongoOpt = { useNewUrlParser: true };
mongoose.connect(databaseURL, mongoOpt);

http.createServer(app).listen(app.get('port'), function () {
  console.log('Express server listening on port ' + app.get('port'));
}); 