var mongoose = require('mongoose');
var Schema = mongoose.Schema;

//Collection user
var user = new Schema({
	name: String,
	username: String,
	email: String,
	type: String
}, { collection: 'user' });
exports.user = mongoose.model('user', user); 

//Collection events
var events = new Schema({
	type: String,
	end_date_time: String,
	name: String,
    start_date_time: String,
    user_id:{type:Schema.Types.ObjectId, ref:'user'}
}, { collection: 'events' });
exports.events = mongoose.model('events', events); 